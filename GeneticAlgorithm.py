import sys

sys.path.insert(0, 'C:/Users/umeyr/Desktop/deeplocation/Lib/site-packages')

import numpy as np
import pandas as pd
from importcsv2 import multiple_import_data

# Test_data = np.around(np.random.randint(RSSI_range[1], 0, size=(100, 10)), decimals=-1)
# Test_result = np.around(np.random.randint(2, size=(100, 1)))

# import test_data from csv file for beacons
files = ["friday 9.40-12.30 inside.csv", "firday 9.40 - 12.00 outside.csv"]
is_insides = [True, False]
mac_addresses = [{"e2:02:00:02:a1:40": 0, "e3:ab:b7:9e:4d:ac": 1},  {'e0:fb:20:d8:49:b5': 0, 'e4:52:35:32:e3:c1': 1, 'd8:1b:d3:1c:01:33': 2, 'e3:2b:1b:21:c0:13': 3}]
Test_data, Test_label = multiple_import_data(files, is_insides, mac_addresses)

"""
# import test_data from csv file for wifi
files = ["monday 16.50- 18.30  inside.csv", "monday 15.05 - 16.40 outside.csv"]
is_insides = [True, False]
mac_addresses = [{"c4:0b:cb:86:9e:c8": 0, "5c:51:88:bc:67:b6": 1, "e8:50:8b:49:5f:52": 2, "24:18:1d:df:64:3c": 3, "74:e2:f5:52:33:94": 4, "68:09:27:25:cc:05": 5},
                 {"c4:0b:cb:86:9e:c8": 0, "5c:51:88:bc:67:b6": 1, "e8:50:8b:49:5f:52": 2, "24:18:1d:df:64:3c": 3, "74:e2:f5:52:33:94": 4, "68:09:27:25:cc:05": 5}]
Test_data, Test_label = multiple_import_data(files, is_insides, mac_addresses)
"""
#Test_data = abs(Test_data.astype(int))
#Test_label = abs(Test_label.astype(int))
#np.savetxt("test_data.csv", Test_data, delimiter=",")
#np.savetxt("test_label.csv", Test_label, delimiter=",")

pd.DataFrame(Test_data).to_csv("test_data.csv")
pd.DataFrame(Test_label).to_csv("test_label.csv")


def genetic_algorithm(test_data, test_label, population_size=50, iteration=1000, rssi_range=np.array([0, -130])):

    numb_device = test_data.shape[1]
    numb_example = test_data.shape[0]

    population = initial_population(population_size, numb_device, rssi_range)

    best_individual, best_accuracy = choose_best(population, 1, test_data, test_label)
    print("Initial Population: " + str(best_accuracy))
    print(best_individual)
    last_accuracy = best_accuracy
    count = 0

    # iterate until stopping criteria satisfied
    for i in range(iteration):
        population, best_accuracy, best_individual = next_generation(population, test_data, test_label, rssi_range)
        print("Generation " + str(i+1) + " : " + str(best_accuracy))
        print(best_individual)
        if best_accuracy > 0.99:
            break
        if last_accuracy == best_accuracy:
            count += 1
            if count > 300:
                break
        last_accuracy = best_accuracy
    return best_individual


def initial_population(population_size, individual_size, rssi_range):
    # initialize the population randomly
    population = np.around(np.random.randint(rssi_range[1], 0, size=(population_size, individual_size)), decimals=-1)

    # initialize the population with all 100
    population = np.zeros((population_size, individual_size))
    population[:, :] = -100
    return population


def fitness(individual_predict, test_label, individual):

    accuracy = np.sum(np.equal(individual_predict, test_label)) / individual_predict.shape[0]
    # in order to force the threshold to be bigger than 50
    accuracy -= np.sum(individual > -50)/individual.size/50
    return accuracy


def predict_zone(individual, test_data):

    predicted_zones = np.sum(individual < test_data, axis=1, keepdims=True) > 0
    return predicted_zones


def next_generation(population, test_data, test_label, rssi_range):

    offspring_size = population.shape[0]//2
    parents_size = population.shape[0] - offspring_size

    assert type(parents_size) == int, "Error in division"

    # choose best individual for parents
    parents, __ = choose_best(population, parents_size, test_data, test_label)
    # choose random couples among parents
    couples = np.random.random_integers(0, parents_size-1, size=(offspring_size, 2))
    offsprings = np.zeros((offspring_size, population.shape[1]))

    for i in range(offspring_size):
        offsprings[i, :] = mate(parents[couples[i, 0]], parents[couples[i, 1]])
        offsprings[i, :] = mutate(offsprings[i, :], rssi_range)

    generation = np.append(parents, offsprings, axis=0)
    best_individual, best_accuracy = choose_best(generation, 1, test_data, test_label)

    return generation, best_accuracy, best_individual


def choose_best(population, best_n, test_data, test_label):

    accuracy_scores = np.zeros(population.shape[0])

    for i in range(population.shape[0]):
        accuracy_scores[i] = fitness(predict_zone(population[i, :], test_data), test_label, population[i, :])

    ind = np.argpartition(accuracy_scores, -best_n)[-best_n:]
    best_index = ind[np.argsort(accuracy_scores[ind])]

    return population[best_index, :], accuracy_scores[best_index]


def mate(parent1, parent2):
    length = parent1.size
    # crossover on the middle
    # offspring = np.append(parent1[0:length//2], parent2[length//2:])

    # crossover on a random interval
    offspring = parent2
    interval = np.random.randint(0, length, size=2)

    if interval[0] < interval[1]:
        offspring[interval[0]:interval[1]] = parent1[interval[0]:interval[1]]
    else:
        offspring[interval[1]:interval[0]] = parent1[interval[1]:interval[0]]

    assert (offspring.size == length), "Error in mate function"
    return offspring


def mutate(individual, rssi_range):

    length = individual.size
    number_iteration = int(np.ceil(length/10)) + 1
    for i in range(number_iteration):
        # random threshold with stepsize 10
        # individual[np.random.randint(length)] = np.around(np.random.randint(rssi_range[1], 0), decimals=-1)
        # random threshold with stepsize 5
        # individual[np.random.randint(length)] = np.around(np.random.randint(rssi_range[1], 0)*2, decimals=-1)/2
        # random threshold with stepsize 1
        individual[np.random.randint(length)] = np.random.randint(rssi_range[1], 0)
    return individual


if __name__ == "__main__":
    weights = genetic_algorithm(Test_data, Test_label)

